﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CustomFunctions {
	[MenuItem ("NEEEU/Open Player Prefs #%p")]
	public static void OpenPlayerPrefs(){
		EditorApplication.ExecuteMenuItem("Edit/Project Settings/Player");
	}
}
#endif
