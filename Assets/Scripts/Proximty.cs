﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proximty : MonoBehaviour {
	public float distance;
	Transform player;
	Animator animator;
	void Start(){
		animator = GetComponent<Animator>();
		player = Camera.main.transform;
	}
	void Update () {
		float currentDistance = Vector3.Distance(player.position,transform.position);
		animator.SetBool("isClose",currentDistance < distance);
	}
}
